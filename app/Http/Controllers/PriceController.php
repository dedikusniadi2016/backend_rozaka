<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PriceController extends Controller
{
    public function index() {
        $price=\App\price::all();
        return view('price/index',compact('price'));    
    }

    public function store(Request $request)
    {
        $price = new \App\price;
        $price->title = $request->get('title');
        $price->price = $request->get('price');
        $price->description = $request->get('description');
        $price->save();
        return redirect('price')->with('success', 'Data harga telah ditambahkan');     
    }
    
    public function create()
    {
        return view('price/create');
    }

    public function destroy($id)
    {
        $price = \App\price::find($id);
        $price->delete();
        return redirect('price')->with('success','Data harga telah di hapus');
    }

    public function edit($id)
    {
        $price = \App\price::find($id);
        return view('price/edit',compact('price','id'));   
    }

    public function update(Request $request, $id)
    {
        $price = \App\price::find($id);
        $price->title = $request->get('title');
        $price->price = $request->get('price');
        $price->description = $request->get('description');
        $price->save();
        return redirect('price')->with('success', 'Data harga telah diubah');     
    }
}
