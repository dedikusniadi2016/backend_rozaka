@extends('layouts.app')
@section('content')
<body>		
		<div class="wrapper d-flex align-items-stretch">
			<nav id="sidebar">
				<div class="custom-menu">
					<button type="button" id="sidebarCollapse" class="btn btn-primary">
	          <i class="fa fa-bars"></i>
	          <span class="sr-only">Toggle Menu</span>
	        </button>
        </div>
				<div class="p-4 pt-5">
		  		<h1><a href="index.html" class="logo">Rozaka</a></h1>
          <ul class="list-unstyled components mb-5">
          <li>
                <a href="{{route('home')}}">Home</a>
	          </li>
	          <li>
                <a href="{{ action("PagesController@index")}}">Pages</a>
            </li>
            
            <li>
            <a href="{{ action("PriceController@index")}}">Price</a>
            </li>
    
	            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
	            </ul>
          </div>          
    	</nav>

      <div id="content" class="p-4 p-md-5 pt-5">
      <div class="container">
        <h3> Price </h3>
        <hr>
      <td><a href="{{action('PriceController@create')}}" class="btn btn-primary">Add</a></td>

    <br /><br>
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Price</th>
        <th>Description</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
      
      @foreach($price as $price)
      <tr>
        <td>{{$price['id']}}</td>
        <td>{{$price['title']}}</td>
        <td>{{$price['price']}}</td>
        <td>{{$price['description']}}</td>
            
        <td><a href="{{action('PriceController@edit', $price['id'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('PriceController@destroy', $price['id'])}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
      </div>
    </div>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/popper.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }} "></script>
    <script src="{{ asset('js/main.js') }} "></script>
  </body>
@endsection